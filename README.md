# Markdown

### Style
> *in nghiêng*

> **bôi đậm**

> ***vừa in nghiêng vừa bôi đậm***
<div style="text-align: right"> ***Link*** </div>
`inlide code`

### Link
[Markdown](http://https://vi.wikipedia.org/wiki/Markdown)

![markdown](https://chiase24.com/wp-content/uploads/2019/07/Tong-hop-hinh-anh-gai-xinh-de-thuong-cute-nhat-6-1.jpg)

### List

###### Unordered List
* item 1
* item 2
* item 3
###### Ordered List
1. item 1
2. item 2
3. item 3


### Horizonal rules
***
horizonal rules

### Cú pháp blockquotes là

> text

{@codepen: https://codepen.io/nickmoreton/pen/gbyygq}

### Code
```
var view = new google.visualization.DataView(data);
  view.setColumns([0, 1]);

  var formatter = new google.visualization.NumberFormat({prefix: '$'});
  formatter.format(data, 1); // Apply formatter to second column

  var table = new google.visualization.Table(document.getElementById('table_sort_div'));
  table.draw(data, {width: '100%', height: '100%'});

  var chart = new google.visualization.BarChart(document.getElementById('chart_sort_div'));
  chart.draw(view);

  google.visualization.events.addListener(table, 'sort',
      function(event) {
        data.sort([{column: event.column, desc: !event.ascending}]);
        chart.draw(view);
      });
```
### Json

```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

### Emoji
Gone camping! :tent: Be back soon.